class Menu {
  constructor(hamburger, menu, overlay) {
    this.hamburgers = document.body.querySelectorAll(hamburger);
    this.menu = document.body.querySelector(menu);
    this.menuElements = this.menu.querySelectorAll('.j-menu-element');
    this.overlay = document.querySelector(overlay);
    this.bindEvents();
  }

  bindEvents() {
    [...this.hamburgers, this.overlay].forEach((item) => {
      item.addEventListener('click', this.toggleMenu.bind(this));
    });

    this.menuElements.forEach((element) => {
      element.addEventListener('click', this.toogleSubElements.bind(this));
    });
  }

  toggleMenu() {
    const subCategories = document.querySelectorAll('.j-menu-sub-elements');
    const menuElementsButton = document.querySelectorAll('.j-menu-element-button');

    menuElementsButton.forEach((button) => {
      button.classList.remove('menu__element-button--active');
    });

    subCategories.forEach((subCategory) => {
      subCategory.classList.remove('menu__sub-elements--active');
    });

    this.menu.classList.toggle('menu--active');
    this.overlay.classList.toggle('menu__overlay--active');
  }

  toogleSubElements({ target }) {
    const categoryButton = target
      .closest('.j-menu-element')
      .querySelector('.j-menu-element-button');

    const subCategories = target
      .closest('.j-menu-element')
      .querySelector('.j-menu-sub-elements');


    categoryButton.classList.toggle('menu__element-button--active');
    if (subCategories) {
      subCategories.classList.toggle('menu__sub-elements--active');
    }
  }
}
class Shopping {
  constructor(button, shoppingMenu, overlay) {
    this.button = document.body.querySelector(button);
    this.shoppingMenu = document.body.querySelector(shoppingMenu);
    this.overlay = document.querySelector(overlay);
    this.bindEvents();
  }

  bindEvents() {
    [this.button, this.overlay].forEach((item) => {
      item.addEventListener('click', this.toogleShoppingMenu.bind(this));
    });
  }

  toogleShoppingMenu() {
    this.shoppingMenu.classList.toggle('shopping--active');
    this.overlay.classList.toggle('shopping__overlay--active');
  }
}

/* eslint-disable-next-line no-unused-vars */
const menu = new Menu('.j-menu-hamburger', '.j-menu', '.j-menu-overlay');

/* eslint-disable-next-line no-unused-vars */
const shopping = new Shopping('.j-shopping-button', '.j-shopping', '.j-shopping-overlay');
