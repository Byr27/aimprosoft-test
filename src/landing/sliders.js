class Sliders {
  constructor() {
    this.productsCarousel();
    this.instagramCarousel();
    this.customersCarousel();
  }

  productsCarousel() {
    $('.j-products-carousel').owlCarousel({
      stagePadding: 50,
      loop: false,
      margin: 10,
      width: 180,
      nav: true,
      dots: false,
      responsive: {
        0: {
          items: 1,
          loop: true,
        },
        600: {
          items: 3,
          loop: true,
        },
        1000: {
          items: 6,
        },
      },
    });
  }

  instagramCarousel() {
    $('.j-instagram-carousel').owlCarousel({
      stagePadding: 50,
      loop: false,
      margin: 10,
      width: 180,
      nav: false,
      dots: false,
      responsive: {
        0: {
          items: 1,
          loop: true,
        },
        600: {
          items: 2,
          loop: true,
        },
        1000: {
          items: 5,
        },
      },
    });
  }

  customersCarousel() {
    $('.j-customers-carousel').owlCarousel({
      stagePadding: 50,
      loop: false,
      margin: 10,
      width: 180,
      nav: false,
      dots: false,
      responsive: {
        0: {
          items: 1,
          loop: true,
        },
        600: {
          items: 2,
          loop: true,
        },
        1000: {
          items: 4,
        },
      },
    });
  }
}

/* eslint-disable-next-line no-unused-vars */
const sliders = new Sliders();
